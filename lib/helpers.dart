import 'package:flutter/material.dart';

class Direction {
  Direction();

  static const int Horizontal = -1;
  static const int Vertical = 1;
}

class UIComponent extends StatelessWidget {
  UIComponent(this.elements,
      {this.direction: Direction.Vertical, this.padding: 16});

  final dynamic elements;
  final int direction;
  final dynamic padding;

  @override
  Widget build(BuildContext context) {
    EdgeInsetsGeometry _padding = elements is PageView
        ? 0.0
        : padding is int || padding is double
            ? EdgeInsets.all(padding.toDouble())
            : padding;

    debugPrint(
        "Building UIComponent ${elements.toString()} with padding ${_padding.toString()}");

    return direction == Direction.Horizontal
        ? (new Padding(
            padding: _padding,
            child: Row(children: <Widget>[
              elements is List
                  ? Columns(elements, direction: direction)
                  : elements
            ]),
          ))
        : (new Padding(
            padding: _padding,
            child: Column(
              children: <Widget>[
                elements is List
                    ? Rows(elements, direction: direction)
                    : elements
              ],
            ),
          ));
  }
}

class UIScaffold extends StatelessWidget {
  UIScaffold(this.child, {this.appBar, this.bottomNavigationBar});
  final Widget child;
  final dynamic appBar;
  final BottomNavigationBar bottomNavigationBar;

  @override
  Widget build(BuildContext context) {
//    debugPrint(
//        "Building scaffold with bottomNav ${bottomNavigationBar.toString()}");
    return new Scaffold(
      appBar: appBar is AppBar ? appBar : null,
      bottomNavigationBar: bottomNavigationBar,
      body: new SafeArea(
        child: child,
      ),
    );
  }
}
//
//Widget _fastScript(List input, {direction: Direction.Vertical}) {
//  return input.reduce((a, b) => Rows(a));
//}

class UI extends StatelessWidget {
  UI(this.data,
      {this.bottomNavigationBar,
      this.direction: Direction.Vertical,
      this.appBar,
      this.padding: 16,
      this.defaultTab: 0,
      this.key})
      : super(key: key);
  final dynamic data;
  final int direction;
  final dynamic padding;
  final dynamic appBar;
  final dynamic bottomNavigationBar;
  final int defaultTab;
  final ObjectKey key;

  PageController _controller;
  int _bottomNavLength;
  int _index;

  @override
  Widget build(BuildContext context) {
    debugPrint('UIComponent: ${(appBar is List && data is List).toString()}');

    final int _bLen =
        bottomNavigationBar is List && bottomNavigationBar.length >= 0
            ? bottomNavigationBar.length
            : 0;
    final int _cLen = data is List && data.length >= 0 ? data.length : 0;

    _bottomNavLength = (_bLen < _cLen ? _bLen : _cLen);
    _index = defaultTab;

    _controller = PageController(initialPage: defaultTab);

    return UIScaffold(
      UIComponent(
          appBar is List && data is List
              ? [
                  Cell(appBar),
                  [Cell(data), 9]
                ]
              : data,
          padding: padding,
          direction: direction),
      appBar: appBar,
      bottomNavigationBar: bottomNavigationBar is List
          ? BottomNavigationBar(
              onTap: (int _val) {
                _controller.animateToPage(_val,
                    duration: Duration(milliseconds: 350),
                    curve: Curves.easeInOut);
              },
              currentIndex: _index,
              items: bottomNavigationBar
                  .take(_bottomNavLength)
                  .map<BottomNavigationBarItem>((x) => BottomNavigationBarItem(
                      icon: Icon(x.any((y) => y is IconData)
                          ? x.where((y) => y is IconData).first
                          : null),
                      title: Text(x.any((y) => y is String)
                          ? x.where((y) => y is String).first
                          : null)))
                  .toList(),
            )
          : (bottomNavigationBar is Widget ? bottomNavigationBar : null),
//        data is List
//        ? _fastScript(data, direction: direction) :
    );
  }
}

class DirectionalElements extends StatelessWidget {
  DirectionalElements(
    this.elements, {
    this.direction,
    this.flex,
    this.mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    this.crossAxisAlignment: CrossAxisAlignment.center,
  });

  final List elements;
  final int direction, flex;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;

  @override
  Widget build(BuildContext context) {
    List _ints(List x) => x.where((y) => y is int).toList();
    List _nonInts(List x) => x.where((y) => !(y is int)).toList();
//    debugPrint("Filling ${(direction == Direction.Horizontal)
//          ? 'horizontally'
//          : ((direction == Direction.Vertical) ? "vertically" : "unknown direction")}");
//    debugPrint(
//        'DirectionalElements called with flex ${flex.toString()}, elements to fill: ${elements.map((x) => (x is Rows || x is Columns) ? "${x.toString()}: flex ${x._flex.toString()}" : x.toString()).toList().toString()}');

    // Rows function
    return (direction == Direction.Horizontal)
        ? Expanded(
            child: Row(
                mainAxisAlignment: mainAxisAlignment,
                crossAxisAlignment: crossAxisAlignment,
                children: elements
                    .where((x) => !(x is int))
                    .map((x) => x is String ? Text(x) : x)
                    .map((x) => Expanded(
                        flex: (x is List)
                            ? ((_ints(x).length > 0 ? _ints(x).first : 1))
                            : ((x is Map && x["flex"] != null)
                                ? x["flex"]
                                : ((x is Rows || x is Columns) ? x._flex : 1)),
                        child: Column(
                          children: [
                            (x is List)
                                ? Rows(x)
                                : (!(x is Widget)
                                    ? ((x["rows"] != null)
                                        ? Rows(x["rows"].toList())
                                        : ((x["cols"] != null)
                                            ? Columns(x["cols"].toList())
                                            : Container()))
                                    : x),
                          ],
                          mainAxisAlignment: mainAxisAlignment,
                          crossAxisAlignment: crossAxisAlignment,
                        )))
                    .toList()))

        // ROWS function
        : (direction == Direction.Vertical)
            ? Expanded(
                child: Column(
                    mainAxisAlignment: mainAxisAlignment,
                    crossAxisAlignment: crossAxisAlignment,
                    children: elements
                        .where((x) => !(x is int))
                        .map((x) => x is String ? Text(x) : x)
//                .map((x) => Expanded(child: x))
                        .map((x) => Expanded(
                              flex: (x is List)
                                  ? (_ints(x).length > 0 ? _ints(x).first : 1)
                                  : ((x is Map && x["flex"] != null)
                                      ? x["flex"]
                                      : ((x is Rows || x is Columns)
                                          ? x._flex
                                          : 1)),
                              child: Row(
                                children: [
                                  (x is List)
                                      ? Columns(x)
                                      : (!(x is Widget)
                                          ? ((x["cols"] != null)
                                              ? Columns(x['cols'].toList())
                                              : ((x["rows"] != null)
                                                  ? Rows(x["rows"].toList())
                                                  : Container()))
                                          : x),
                                ],
                                mainAxisAlignment: mainAxisAlignment,
                                crossAxisAlignment: crossAxisAlignment,
                              ),
                            ))
                        .toList()))
            : Container();
  }

  get _flex {
    int val = elements.any((x) => x is int)
        ? elements.where((x) => x is int).toList()[0]
        : this.flex;
//    debugPrint("this._flex (returned): ${val}, this.flex: ${flex} ");
    return val;
  }
}

class Rows extends DirectionalElements {
  Rows(
    this.columns, {
    this.flex: 1,
    this.direction: Direction.Vertical,
    this.mainAxisAlignment: MainAxisAlignment.spaceAround,
    this.crossAxisAlignment: CrossAxisAlignment.center,
  }) : super(
          columns,
          flex: flex,
          mainAxisAlignment: mainAxisAlignment,
          crossAxisAlignment: crossAxisAlignment,
          direction: Direction.Vertical,
        );
  final List columns;
  final int flex;
  final int direction;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;

  @override
  Widget build(BuildContext context) {
//    debugPrint("Building rows (flex ${this._flex}) ${columns.toString()}");
    return super.build(context);
  }
}

class Columns extends DirectionalElements {
  Columns(
    this.rows, {
    this.flex: 1,
    this.direction: Direction.Horizontal,
    this.mainAxisAlignment: MainAxisAlignment.spaceAround,
    this.crossAxisAlignment: CrossAxisAlignment.center,
  }) : super(
          rows,
          flex: flex,
          mainAxisAlignment: mainAxisAlignment,
          crossAxisAlignment: crossAxisAlignment,
          direction: direction,
        );
  final List rows;
  final int flex;
  final int direction;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;

  @override
  Widget build(BuildContext context) {
//    debugPrint("Building columns (flex ${this._flex}) ${rows.toString()}");
    return super.build(context);
  }
}

class Cell extends StatelessWidget {
  Cell(this.child,
      {this.style,
      this.flex: 1,
      this.color: Colors.transparent,
      this.padding: 8.0});
  final dynamic child;
  final dynamic style;
  final Color color;
  final dynamic padding;
  final int flex;

  @override
  Widget build(BuildContext context) {
    // If padding is straight numerical, assume EdgeInsets.all.
    // Otherwise, use supplied value as given.

    final EdgeInsetsGeometry _padding = (padding is double || padding is int)
        ? EdgeInsets.all(padding.toDouble())
        : padding;

    return (child is! RaisedButton &&
            child is! FlatButton &&
            child is! MaterialButton &&
            child is! OutlineButton
        ? (child is List
            ? Expanded(
                child: UIComponent(
                child,
                padding: _padding,
              ))
            : Expanded(
                child: UIComponent(
                child is String
                    ? [
                        FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            child,
                            style: style,
                            textAlign: TextAlign.center,
                          ),
                        )
                      ]
                    : [FittedBox(fit: BoxFit.scaleDown, child: child)],
                padding: _padding,
              )))
        : Expanded(
            child: Padding(
                child: Material(
                  color: Colors.white,
                  elevation: 2.0,
                  child: child,
                ),
                padding: _padding)));
  }
}

class Pages extends StatefulWidget {
  Pages(this.children,
      {this.appBar, this.bottomNavigationBar, this.outsidePadding});
  final dynamic children;
  final dynamic appBar;
  final dynamic bottomNavigationBar;
  final dynamic outsidePadding;

  @override
  _PagesState createState() => new _PagesState(children,
      appBar: appBar,
      bottomNavigationBar: bottomNavigationBar,
      outsidePadding: outsidePadding);
}

class _PagesState extends State<Pages> with TickerProviderStateMixin {
  _PagesState(this.children,
      {this.appBar,
      this.bottomNavigationBar,
      this.defaultTab: 0,
      this.outsidePadding});

  PageController _controller;
  int _index;
  int _bottomNavLength;

  final dynamic children;
  final dynamic appBar;
  final dynamic bottomNavigationBar;
  final int defaultTab;
  final dynamic outsidePadding;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    debugPrint('Index is changing: ${_controller.indexIsChanging.toString()}');
    return Container(
      width: MediaQuery.of(context).size.width -
          (outsidePadding != null &&
                  (outsidePadding is int || outsidePadding is double)
              ? 2.0 * outsidePadding.toDouble()
              : 32),
      height: MediaQuery.of(context).size.height,
      child: PageView(
        children: children.map<Widget>((x) => UI(x, padding: 0)).toList(),
        controller: _controller,
        onPageChanged: (int _val) => setState(() => _index = _val),
      ),
    );
//        appBar: appBar,
//        bottomNavigationBar: bottomNavigationBar is List
//            ? BottomNavigationBar(
//                onTap: (int _val) {
//                  _controller.animateToPage(_val,
//                      duration: Duration(milliseconds: 350),
//                      curve: Curves.easeInOut);
//                },
//                currentIndex: _index,
//                items: bottomNavigationBar
//                    .take(_bottomNavLength)
//                    .map<BottomNavigationBarItem>((x) =>
//                        BottomNavigationBarItem(
//                            icon: Icon(x.any((y) => y is IconData)
//                                ? x.where((y) => y is IconData).first
//                                : null),
//                            title: Text(x.any((y) => y is String)
//                                ? x.where((y) => y is String).first
//                                : null)))
//                    .toList(),
//              )
//            : (bottomNavigationBar is Widget ? bottomNavigationBar : null));
  }
}
