//import 'package:flutter/material.dart';
//
//class StdPad extends Padding {
//  StdPad({this.child}) : super(padding: EdgeInsets.all(8.0), child: child);
//
//  final Widget child;
//}
//
//class HvyPad extends Padding {
//  HvyPad({this.child}) : super(padding: EdgeInsets.all(16.0), child: child);
//
//  final Widget child;
//}
//
//class ShrinkWrap extends FittedBox {
//  ShrinkWrap({this.child}) : super(fit: BoxFit.scaleDown, child: child);
//
//  final Widget child;
//}
//
//class SingleColumn extends Column {
//  SingleColumn({this.child}) : super(children: [child]);
//
//  final Widget child;
//}
//
//class SingleRow extends Row {
//  SingleRow({this.child}) : super(children: [child]);
//
//  final Widget child;
//}
//
//class LayoutText extends StatelessWidget {
//  LayoutText(this.text,
//      {this.style: const TextStyle(), this.insidePadding: 0.0});
//
//  @override
//  Widget build(BuildContext context) {
//    return new Padding(
//      padding: EdgeInsets.all(insidePadding),
//      child: new Center(
//          child: Text(
//        text,
//        style: style,
//        maxLines: 2,
//        overflow: TextOverflow.ellipsis,
//        textAlign: TextAlign.center,
//      )),
//    );
//  }
//
//  final String text;
//  final TextStyle style;
//  final double insidePadding;
//}
//
//class Sheet extends Material {
//  Sheet({this.child, this.color = Colors.white})
//      : super(child: child, color: Colors.white, elevation: 16.0);
//
//  final Widget child;
//  final Color color;
//}
//
//class HeaderMainText extends StatelessWidget {
//  HeaderMainText(this.text);
//
//  final String text;
//
//  @override
//  Widget build(BuildContext context) {
//    return LayoutText(
//      text,
//      style: Theme.of(context).textTheme.display1,
//    );
//  }
//}
//
//class HeaderSubText extends StatelessWidget {
//  HeaderSubText(this.text);
//
//  final String text;
//
//  @override
//  Widget build(BuildContext context) {
//    return LayoutText(
//      text,
//      style: Theme.of(context).textTheme.caption,
//    );
//  }
//}
//
//List _expandAndCenter(List children) {
//  return children;
////      .map((Widget child) => Expanded(child: Center(child: child)))
////      .toList();
//}
//
//class FullCenter extends Container {
//  FullCenter({this.child})
//      : super(
//          child: Center(
//            child: child,
//          ),
//        );
//
//  final Widget child;
//}
//
//class HorizontalFillSingle extends HorizontalFill {
//  HorizontalFillSingle(this.child, {this.flex: 1})
//      : super(
//          [
//            HorizontalLayout([child])
//          ],
//          flex: flex,
//        );
//
//  final Widget child;
//  final int flex;
//}
//
//class HorizontalLayoutSingle extends HorizontalLayout {
//  HorizontalLayoutSingle(this.child, {this.insidePadding: 0.0})
//      : super(_expandAndCenter([
//          new Padding(
//            padding: EdgeInsets.all(insidePadding),
//            child: child,
//          )
//        ]));
//
//  final Widget child;
//  final double insidePadding;
//}
//
//class VerticalFillSingle extends VerticalFill {
//  VerticalFillSingle(this.child, {this.flex: 1})
//      : super(
//          [
//            VerticalLayout([child])
//          ],
//          flex: flex,
//        );
//
//  final Widget child;
//  final int flex;
//}
//
//class VerticalLayoutSingle extends VerticalLayout {
//  VerticalLayoutSingle(this.child, {this.insidePadding: 0.0})
//      : super(_expandAndCenter([
//          new Padding(
//            padding: EdgeInsets.all(insidePadding),
//            child: child,
//          )
//        ]));
//
//  final Widget child;
//  final double insidePadding;
//}
//
//class VerticalFill extends StatelessWidget {
//  VerticalFill(this.children, {this.mode, this.flex: 1});
//
//  final dynamic children;
//  final int flex;
//  final int mode;
//
//  @override
//  Widget build(BuildContext context) {
////    debugPrint("VertFilling ${children.toString()}, mode ${_modePrint(mode)}");
//    final List _children = children is Widget ? <Widget>[children] : children;
//    // TODO: implement build
//    return Expanded(
//      child: VerticalLayout(
//        _children,
//        mode: mode,
//      ),
//      flex: flex,
//    );
//  }
//}
//
//class HorizontalFill extends StatelessWidget {
//  HorizontalFill(this.children, {this.mode, this.flex: 1});
//
//  final dynamic children;
//  final int flex;
//  final int mode;
//
//  @override
//  Widget build(BuildContext context) {
////    debugPrint("HorizFilling ${children.toString()}, mode ${_modePrint(mode)}");
//    final List _children = children is Widget ? <Widget>[children] : children;
//    // TODO: implement build
//    return Expanded(
//      child: HorizontalLayout(
//        _children,
//        mode: mode,
//      ),
//      flex: flex,
//    );
//  }
//}
//
//class DirectionalLayout extends StatelessWidget {
//  DirectionalLayout(this.inputs,
//      {this.direction, this.mode, this.insidePadding: 0.0});
//  final int direction; // 0 is horizontal, 1 is vertical
//  final int mode;
//
//  final dynamic inputs;
//  final double insidePadding;
//  static const int Horizontal = 0;
//  static const int Vertical = 1;
//
//  @override
//  Widget build(BuildContext context) {
////    debugPrint("${direction == DirectionalLayout.Horizontal
////            ? "Horizontally"
////            : (direction == DirectionalLayout.Vertical
////              ? "Vertically"
////              : "[Direction Unknown]")} laying out ${inputs.toString()}, mode ${_modePrint(mode)}");
//    final List<Widget> _inputs = _process(inputs, mode);
//
//    return (direction == 0)
//        ? Row(
//            children: _inputs,
//            crossAxisAlignment: CrossAxisAlignment.center,
//            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//          )
//        : Column(
//            children: _inputs,
//            crossAxisAlignment: CrossAxisAlignment.center,
//            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//          );
//    ;
//  }
//}
//
//class HorizontalLayout extends DirectionalLayout {
//  HorizontalLayout(this.columns, {this.mode, this.insidePadding: 0.0})
//      : super(
//          columns,
//          mode: mode,
//          direction: DirectionalLayout.Horizontal,
//          insidePadding: insidePadding,
//        );
//
//  final dynamic columns;
//  final double insidePadding;
//  final int mode;
//}
//
//class VerticalLayout extends DirectionalLayout {
//  VerticalLayout(this.rows, {this.mode, this.insidePadding: 0.0})
//      : super(
//          rows,
//          mode: mode,
//          direction: DirectionalLayout.Vertical,
//          insidePadding: insidePadding,
//        );
//
//  final dynamic rows;
//  final double insidePadding;
//  final int mode;
//}
//
//bool _testForFlex(List tree) =>
//    tree.any((y) => y is List && y.any((x) => x is int));
//
//List<Widget> _typeSafety(List inputs) {
////  debugPrint("_typeSafety: ${inputs.toString()}");
//  return inputs.map<Widget>((x) => x).toList();
//}
//
//void _modePrint(mode) => mode == DirectionalLayout.Horizontal
//    ? 'H'
//    : (mode == DirectionalLayout.Vertical ? 'V' : mode.toString());
//
//Widget _expand(element, {mode, flex: 1}) {
////  debugPrint(
////      "expanding ${element.toString()}, mode ${_modePrint(mode)}, flex ${flex.toString()}");
////  if (element is Text) element = Center(child: element);
//
//  return mode == DirectionalLayout.Vertical
//      ? VerticalFill(HorizontalFill(element, mode: mode),
//          flex: flex, mode: mode)
//      : HorizontalFill(VerticalFill(element, mode: mode),
//          flex: flex, mode: mode);
//}
//
//List<Widget> _process(dynamic inputs, mode) {
////  debugPrint("processing ${inputs.toString()}, mode ${_modePrint(mode)}");
////  debugPrint("_testForFlex: ${_testForFlex(inputs)}");
//  return _typeSafety(inputs is Widget // check if lone widget
//      ? <Widget>[inputs] // wrap if so
//      // check for flex in tree
//      : (inputs is List && inputs.length > 1 && _testForFlex(inputs))
//          // there's a flex specified for one of the tree nodes
//          ? inputs
//              .where((x) => !(x is int))
//              .map((x) => (x is List && x.any((y) => y is int))
//                  ? _expand(x,
//                      flex: x.where((y) => y is int).length >= 1
//                          ? x.where((y) => y is int).toList()[0]
//                          : 1,
//                      mode: mode == DirectionalLayout.Vertical
//                          ? DirectionalLayout.Horizontal
//                          : DirectionalLayout.Vertical)
//                  : _expand(x,
//                      mode: mode == DirectionalLayout.Vertical
//                          ? DirectionalLayout.Horizontal
//                          : DirectionalLayout.Vertical))
//              .toList()
//          // no flex
//          : ((inputs is List &&
//                  inputs.length == 1) // lone Widget signifies a tree root
//              ? ((inputs[0] is List)
//                  ? [
//                      _expand(inputs[0],
//                          mode: mode == DirectionalLayout.Vertical
//                              ? DirectionalLayout.Horizontal
//                              : DirectionalLayout.Vertical)
//                    ]
//                  : inputs) // package as a List<Widget> and end recursion
//              : inputs // otherwise iterate over the list, descend down the tree
//                  .where((x) => !(x is int))
//                  // FLEX HERE
//                  .map((x) => _expand(x,
//                      mode: mode == DirectionalLayout.Vertical
//                          ? DirectionalLayout.Horizontal
//                          : DirectionalLayout.Vertical))
//                  .toList()));
//}
//
//class UIScaffold extends StatelessWidget {
//  UIScaffold(this.child);
//  final Widget child;
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      body: new SafeArea(
//        child: child,
//      ),
//    );
//  }
//}
//
//class UIBuilder extends StatelessWidget {
//  UIBuilder(this.columns);
//
//  final List columns;
//
//  @override
//  Widget build(BuildContext context) {
//    return VerticalLayout(columns, mode: DirectionalLayout.Horizontal);
//  }
//}
//
//class UI extends StatelessWidget {
//  UI(this.data);
//  final Widget data;
//
//  @override
//  Widget build(BuildContext context) {
//    return UIScaffold(data is List
//        ? Container()
//        : (data is Columns ? Row(children: [data]) : Column(children: [data])));
////    return UIScaffold(
////      UIBuilder(data),
////        );
//  }
//}
//
//class DirectionalElements extends StatelessWidget {
//  DirectionalElements(
//    this.elements, {
//    this.direction,
//    this.flex,
//    this.mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//    this.crossAxisAlignment: CrossAxisAlignment.center,
//  });
//
//  final List elements;
//  final int direction, flex;
//  final MainAxisAlignment mainAxisAlignment;
//  final CrossAxisAlignment crossAxisAlignment;
//
//  @override
//  Widget build(BuildContext context) {
//    debugPrint(
//        'DirectionalElements called with flex ${flex.toString()}, elements to fill: ${elements.map((x) => (x is Rows || x is Columns) ? "${x.toString()}: flex ${x._flex.toString()}" : x.toString()).toList().toString()}');
//    return direction == DirectionalLayout.Horizontal
//        ? Expanded(
//            child: Row(
//                mainAxisAlignment: mainAxisAlignment,
//                crossAxisAlignment: crossAxisAlignment,
//                children: elements
//                    .where((x) => (x is Widget))
////                .map((x) => Expanded(child: x))
//                    .map((x) => Expanded(
//                        flex: (x is Rows || x is Columns) ? x._flex : 1,
//                        child: Column(
//                          children: [x],
//                          mainAxisAlignment: mainAxisAlignment,
//                          crossAxisAlignment: crossAxisAlignment,
//                        )))
//                    .toList()))
//        : Expanded(
//            child: Column(
//                mainAxisAlignment: mainAxisAlignment,
//                crossAxisAlignment: crossAxisAlignment,
//                children: elements
//                    .where((x) => (x is Widget))
////                .map((x) => Expanded(child: x))
//                    .map((x) => Expanded(
//                        flex: (x is Rows || x is Columns) ? x._flex : 1,
//                        child: Row(
//                          children: [x],
//                          mainAxisAlignment: mainAxisAlignment,
//                          crossAxisAlignment: crossAxisAlignment,
//                        )))
//                    .toList()));
//  }
//
//  get _flex {
//    return elements.any((x) => x is int)
//        ? elements.where((x) => x is int).toList()[0]
//        : this.flex;
//  }
//}
//
//class Rows extends DirectionalElements {
//  Rows(
//    this.columns, {
//    this.flex: 1,
//    this.mainAxisAlignment: MainAxisAlignment.spaceAround,
//    this.crossAxisAlignment: CrossAxisAlignment.center,
//  }) : super(
//          columns,
//          mainAxisAlignment: mainAxisAlignment,
//          crossAxisAlignment: crossAxisAlignment,
//          direction: DirectionalLayout.Vertical,
//        );
//  final List columns;
//  final int flex;
//  final MainAxisAlignment mainAxisAlignment;
//  final CrossAxisAlignment crossAxisAlignment;
//}
//
//class Columns extends DirectionalElements {
//  Columns(
//    this.rows, {
//    this.flex: 1,
//    this.mainAxisAlignment: MainAxisAlignment.spaceAround,
//    this.crossAxisAlignment: CrossAxisAlignment.center,
//  }) : super(
//          rows,
//          mainAxisAlignment: mainAxisAlignment,
//          crossAxisAlignment: crossAxisAlignment,
//          direction: DirectionalLayout.Horizontal,
//        );
//  final List rows;
//  final int flex;
//  final MainAxisAlignment mainAxisAlignment;
//  final CrossAxisAlignment crossAxisAlignment;
//}
//
//class Cell extends StatelessWidget {
//  Cell({this.child, this.color: Colors.transparent, this.padding: 0.0});
//  final dynamic child;
//  final Color color;
//  final dynamic padding;
//
//  @override
//  Widget build(BuildContext context) {
//    // If padding is straight numerical, assume EdgeInsets.all.
//    // Otherwise, use supplied value as given.
//
//    final EdgeInsetsGeometry _padding = (padding is double || padding is int)
//        ? EdgeInsets.all(padding.toDouble())
//        : padding;
//
//    return child is Widget
//        ? (child is Text
//            ? Expanded(
//                child: Center(
//                child: Padding(
//                  child: child,
//                  padding: _padding,
//                ),
//              ))
//            : Expanded(child: Padding(child: child, padding: _padding)))
//        : Expanded(
//            child: Padding(
//            padding: _padding,
//            child: Container(
//              color: color,
//            ),
//          ));
//  }
//}
