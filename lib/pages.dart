import 'package:flutter/material.dart';

import 'helpers.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return UI(
      [
        'test',
        Pages(
          [
            ['test', 'test'],
            ['test'],
            [
              ['test'],
              ['test']
            ]
          ],
        ),
      ],
      bottomNavigationBar: [
        [Icons.cloud_download, "Browse Offers"],
        [Icons.timer, "Right Now"],
        [Icons.cloud_upload, "Post a Request"]
      ],
    );
//    return UI(
//      [
//        Cell('TEST', style: Theme.of(context).textTheme.display4),
//        [
//          [
//            Cell(
//              _counter.toString(),
//              style: Theme.of(context).textTheme.display4,
//            ),
//            Cell(RaisedButton(
//                child: Text('REFRESH'), onPressed: _incrementCounter))
//          ],
//          9
//        ]
//      ],
//      appBar: [new Text('test')],
//      bottomNavigationBar: BottomNavigationBar(
//        items: [
//          BottomNavigationBarItem(
//              icon: Icon(Icons.cloud_upload), title: Text('Offers')),
//          BottomNavigationBarItem(
//              icon: Icon(Icons.timer), title: Text('Right Now')),
//          BottomNavigationBarItem(
//              icon: Icon(Icons.cloud_download), title: Text('Requests')),
//        ],
//      ),
//    );
  }
}
