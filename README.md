# flutter-ui

A demo application displaying the use of the flutter-ui library.  

## Purpose and Usage

The library offers a convenient shorthand for laying out complex, responsive layouts in Dart using Google's Flutter framework.  For example, here is a responsive layout that covers the full width of the device screen.  It shows two elements, side-by-side, and each taking up 50% of the screen width - the number 1 is displayed in the left box, the number 2 in the right.


```
new Expanded(
	child: new Row(
    	children: [
        	new Expanded(child: Column(children: [
            	new Text('1')
            ]),
            new Expanded(child: new Column(children: [
            	new Text('2')
            ])
        ]
    )
)
```
That's an awful lot of typing.  With the flutter-ui library, soon to be released (which can be explored in this project), it is as simple as:
```
new UI(['1', '2'])
```
One set of braces indicates columns.  Two sets indicates rows.  For example, `UI(['1', ['2', '3']])` splits the rightmost box into two horizontal columns showing the numbers 2 and 3, while `UI(['1', [['2', '3']] ])` splits the rightmost box into two verical rows.